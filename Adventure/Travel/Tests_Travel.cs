﻿using NUnit.Framework;

namespace ConsoleRPG.Adventure.Travel
{
  [TestFixture]
  public class Tests_TravelModel
  {
    [Test]
    public void Test_ModelConstructorNull ()
    {
      TravelModel model = new TravelModel ();
      Assert.AreEqual (
        GameLocation.Home,
        model.GetLocation ());
    }

    [Test]
    public void Test_ModelConstructorNonNull ()
    {
      TravelModel model = new TravelModel (GameLocation.Arena);
      Assert.AreEqual (
        GameLocation.Arena,
        model.GetLocation ());
    }

    [Test]
    public void Test_ModelSetter ()
    {
      TravelModel model = new TravelModel ();
      model.SetLocation (GameLocation.Dungeon);
      Assert.AreEqual (
        GameLocation.Dungeon,
        model.GetLocation ());
    }
  }

  [TestFixture]
  public class Tests_TravelView
  {
    [Test]
    public void Test_ViewAtHome ()
    {
      TravelModel model = new TravelModel ();
      TravelView view = new TravelView ();

      Assert.AreEqual (
        "You're currently at Home.",
        view.GetCurrentLocation (model));
    }

    [Test]
    public void Test_ViewInCases ()
    {
      TravelModel model = new TravelModel (GameLocation.Arena);
      TravelView view = new TravelView ();

      Assert.AreEqual (
        "You're currently in Arena.",
        view.GetCurrentLocation (model));

      model.SetLocation (GameLocation.Dungeon);
      Assert.AreEqual (
        "You're currently in Dungeon.",
        view.GetCurrentLocation (model));

      model.SetLocation (GameLocation.TradePost);
      Assert.AreEqual (
        "You're currently in TradePost.",
        view.GetCurrentLocation (model));
    }
  }

  [TestFixture]
  public class Tests_TravelController
  {
    [Test]
    public void Test_ControllerConstructor ()
    {
      TravelController controller = new TravelController ();

      Assert.AreEqual (
        "You're currently at Home.",
        controller.GetCurrentLocation ());
    }

    [Test]
    public void Test_ControllerTravel ()
    {
      TravelController controller = new TravelController ();
      controller.TravelTo (GameLocation.Arena);

      Assert.AreEqual (
        "You're currently in Arena.",
        controller.GetCurrentLocation ());
    }
  }
}
