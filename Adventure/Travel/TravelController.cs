﻿namespace ConsoleRPG.Adventure.Travel
{
  public class TravelController
  {
    private TravelModel model;
    private TravelView view;

    public TravelController (GameLocation gameLocation = GameLocation.Home)
    {
      model = new TravelModel (gameLocation);
      view = new TravelView ();
    }

    public void TravelTo (GameLocation gameLocation)
    {
      model.SetLocation (gameLocation);
    }

    public string GetCurrentLocation ()
    {
      return view.GetCurrentLocation (model);
    }
  }
}
