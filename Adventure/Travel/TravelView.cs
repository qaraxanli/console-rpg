﻿namespace ConsoleRPG.Adventure.Travel
{
  public class TravelView
  {
    public string GetCurrentLocation (TravelModel model)
    {
      GameLocation currentGameLocation = model.GetLocation ();
      string preposition;

      if (currentGameLocation == GameLocation.Home)
      {
        preposition = "at";
      }
      else
      {
        preposition = "in";
      }

      return string.Format (
        "You're currently {0} {1}.",
        preposition,
        model.GetLocation ().ToString ());
    }
  }
}
