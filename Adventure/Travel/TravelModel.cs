﻿namespace ConsoleRPG.Adventure.Travel
{
  public class TravelModel
  {
    private GameLocation currentGameLocation;

    public TravelModel (GameLocation _gameLocation = GameLocation.Home)
    {
      currentGameLocation = _gameLocation;
    }

    public void SetLocation (GameLocation _gameLocation)
    {
      currentGameLocation = _gameLocation;
    }

    public GameLocation GetLocation ()
    {
      return currentGameLocation;
    }
  }
}
