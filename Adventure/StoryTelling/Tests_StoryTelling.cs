﻿using NUnit.Framework;

namespace ConsoleRPG.Adventure.StoryTelling
{
  [TestFixture ()]
  public class Tests_StoryTellingModel
  {
    [Test ()]
    public void Test_ModelCheckInfoTexts ()
    {
      string[] infoTexts = {
        "What would you like to do before you go out?",
        "Got some stuff to trade before you go out?",
        "Challenges big and small await you here. Dare to give yourself a chance?!",
        "Shit load of monsters will be joining the party. All hail the glorious death!"
      };

      StoryTellingModel model = new StoryTellingModel ();

      for (int i = 0; i < infoTexts.Length; i++)
      {
        Assert.AreEqual (
          infoTexts[i],
          model.GetInfoText ((GameLocation) i));
      }
    }
  }
}
