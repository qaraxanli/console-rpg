﻿using System;

namespace ConsoleRPG
{
  class MainClass
  {
    private static Adventure.Travel.TravelController travel;
    private static Adventure.StoryTelling.StoryTellingController storyTelling;
    private static Player.Input.PlayerInputController playerInput;

    public static void Main (string[] args)
    {
      Console.WriteLine ("Hello to the RPG World!");

      travel = new Adventure.Travel.TravelController ();

      playerInput = new Player.Input.PlayerInputController ();

      int inputChoice = 0;
      while (inputChoice != -1)
      {
        Console.WriteLine ();
        Console.WriteLine (travel.GetCurrentLocation ());

        playerInput.SetPlayerInput (Console.ReadLine ());
        inputChoice = playerInput.GetPlayerInput ();
      }
    }
  }
}
