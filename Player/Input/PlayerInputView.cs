﻿namespace ConsoleRPG.Player.Input
{
  public class PlayerInputView
  {
    public int GetPlayerInput (PlayerInputModel model)
    {
      int result;
      if (int.TryParse (model.GetInput (), out result))
      {
        return result;
      }
      else
      {
        return -1;
      }
    }
  }
}
