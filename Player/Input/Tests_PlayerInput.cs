﻿using NUnit.Framework;
using System;
namespace ConsoleRPG.Player.Input
{
  [TestFixture]
  public class Tests_PlayerInputModel
  {
    [Test]
    public void Test_ModelSetter ()
    {
      PlayerInputModel model = new PlayerInputModel ();
      model.SetInput ("1");

      Assert.AreEqual (
        "1",
        model.GetInput ());
    }
  }

  [TestFixture]
  public class Tests_PlayerInputView
  {
    [Test]
    public void Test_ViewCorrectInput ()
    {
      PlayerInputModel model = new PlayerInputModel ();
      model.SetInput ("a");

      PlayerInputView view = new PlayerInputView ();

      Assert.AreEqual (
        -1,
        view.GetPlayerInput (model));
    }

    [Test]
    public void Test_ViewIncorrectInput ()
    {
      PlayerInputModel model = new PlayerInputModel ();
      model.SetInput ("3");

      PlayerInputView view = new PlayerInputView ();

      Assert.AreEqual (
        3,
        view.GetPlayerInput (model));
    }
  }

  [TestFixture]
  public class Tests_PlayerInputController
  {
    [Test]
    public void Test_ControllerSetCorrectInput ()
    {
      PlayerInputController controller = new PlayerInputController ();
      controller.SetPlayerInput ("3");

      Assert.AreEqual (
        3,
        controller.GetPlayerInput ());
    }

    [Test]
    public void Test_ControllerSetIncorrectInput ()
    {
      PlayerInputController controller = new PlayerInputController ();
      controller.SetPlayerInput ("a15");

      Assert.AreEqual (
        -1,
        controller.GetPlayerInput ());
    }
  }
}
