﻿using System;
namespace ConsoleRPG.Player.Input
{
  public enum InputType
  {
    Number,
    Character,
    String
  }
}
