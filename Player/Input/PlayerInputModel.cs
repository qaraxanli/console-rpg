﻿namespace ConsoleRPG.Player.Input
{
  public class PlayerInputModel
  {
    private string playerInput;

    public void SetInput (string _playerInput)
    {
      playerInput = _playerInput;
    }

    public string GetInput ()
    {
      return playerInput;
    }
  }
}
