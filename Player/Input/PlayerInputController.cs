﻿namespace ConsoleRPG.Player.Input
{
  public class PlayerInputController
  {
    private PlayerInputModel model;
    private PlayerInputView view;

    public PlayerInputController ()
    {
      model = new PlayerInputModel ();
      view = new PlayerInputView ();
    }

    public void SetPlayerInput (string playerInput)
    {
      model.SetInput (playerInput);
    }

    public int GetPlayerInput ()
    {
      return view.GetPlayerInput (model);
    }
  }
}
